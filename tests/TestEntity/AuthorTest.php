<?php

namespace App\Tests\TestEntity;

use App\Entity\Author;
use App\Entity\Book;
use PHPUnit\Framework\TestCase;

class AuthorTest extends TestCase
{
    public function testGetId(): void
    {
        $author = new Author();
        $this->assertNull($author->getId());
    }
    public function testNames(): void
    {
        $author = new Author();
        $author->setName('John Doe');
        $this->assertSame('John Doe', $author->getName());
    }

    public function testBooks(): void
    {
        $author = new Author();
        $book = new Book();
        $book1 = new Book();

        $author->addBook($book);
        $author->addBook($book1);
        $this->assertTrue($author->getBooks()->contains($book));
        $this->assertTrue($author->getBooks()->contains($book1));
        $author->removeBook($book);
        $this->assertFalse($author->getBooks()->contains($book));

    }



}