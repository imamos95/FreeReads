<?php

namespace App\Tests\TestEntity;

use App\Entity\Book;
use App\Entity\Publisher;
use PHPUnit\Framework\TestCase;

class PublisherTest extends TestCase
{
    public function testGetBooksReturnsEmptyCollectionByDefault(): void
    {
        $publisher = new Publisher();
        $this->assertEmpty($publisher->getBooks());
    }

    public function testAddBookAddsBookToCollection(): void
    {
        $publisher = new Publisher();
        $book = new Book();
        $publisher->addBook($book);
        $this->assertSame($book, $publisher->getBooks()->first());
        $this->assertCount(1, $publisher->getBooks());
    }

    public function testAddBookDoesNotAddDuplicateBookToCollection(): void
    {
        $publisher = new Publisher();
        $book = new Book();
        $publisher->addBook($book);
        $publisher->addBook($book);
        $this->assertSame($book, $publisher->getBooks()->first());
        $this->assertCount(1, $publisher->getBooks());
    }

    public function testRemoveBookRemovesBookFromCollection(): void
    {
        $publisher = new Publisher();
        $book = new Book();
        $publisher->addBook($book);
        $publisher->removeBook($book);
        $this->assertEmpty($publisher->getBooks());
    }

    public function testRemoveBookDoesNotRemoveNonexistentBookFromCollection(): void
    {
        $publisher = new Publisher();
        $book = new Book();
        $publisher->removeBook($book);
        $this->assertEmpty($publisher->getBooks());
    }

    public function testGetAndSetName(): void
    {
        $publisher = new Publisher();
        $publisher->setName('Test Name');
        $this->assertSame('Test Name', $publisher->getName());
    }

    public function testGetId(): void
    {
        $publisher = new Publisher();
        $this->assertNull($publisher->getId());
    }

}