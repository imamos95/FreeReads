<?php

namespace App\Tests\TestEntity;

use App\Entity\Status;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class StatusTest extends TestCase
{

    public function testGetUserBooks(): void
    {
        $status = new Status();
        $userBook = new UserBook();
        $userBook1 = new UserBook();
        $status->addUserBook($userBook);
        $status->addUserBook($userBook1);
        $this->assertCount(2, $status->getUserBooks());

    }

    public function testAddUserBook(): void
    {
        $status = new Status();
        $userBook = new UserBook();
        $status->addUserBook($userBook);
        $this->assertTrue($status->getUserBooks()->contains($userBook));
        $this->assertSame($status, $userBook->getStatus());
    }

    public function testGetAndSetName(): void
    {
        $status = new Status();
        $status->setName('Test Name');
        $this->assertSame('Test Name', $status->getName());
    }

    public function testRemoveUserBook(): void
    {
        $status = new Status();
        $userBook = new UserBook();
        $status->addUserBook($userBook);
        $status->removeUserBook($userBook);
        $this->assertFalse($status->getUserBooks()->contains($userBook));
    }

    public function testGetId(): void
    {
        $status = new Status();
        $this->assertNull($status->getId());
    }

}