<?php

namespace App\Tests\TestEntity;

use App\Entity\Book;
use App\Entity\Status;
use App\Entity\User;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class UserBookTest extends TestCase
{
    public function testGettersAndSetters(): void
    {
        $userBook = new UserBook();
        $user= new User();
        $book = new Book();
        $status = new Status();
        $userBook->setUser($user);
        $userBook->setBook($book);
        $userBook->setStatus($status);
        $userBook->setComment('Test comment');
        $userBook->setRating(5);
        $userBook->setCreatedAt(new \DateTimeImmutable());
        $userBook->setUpdatedAt(new \DateTimeImmutable());
        $this->assertInstanceOf(User::class, $userBook->getUser());
        $this->assertInstanceOf(Book::class, $userBook->getBook());
        $this->assertInstanceOf(Status::class, $userBook->getStatus());
        $this->assertSame('Test comment', $userBook->getComment());
        $this->assertSame(5, $userBook->getRating());
        $this->assertInstanceOf(\DateTimeImmutable::class, $userBook->getCreatedAt());
        $this->assertInstanceOf(\DateTimeImmutable::class, $userBook->getUpdatedAt());
        $this->assertNull($userBook->getId());
    }

}