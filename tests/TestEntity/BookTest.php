<?php

namespace App\Tests\TestEntity;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Publisher;
use App\Entity\UserBook;
use PHPUnit\Framework\TestCase;

class BookTest extends TestCase
{
    public function testGettersAndSetters(): void
    {
        $book = new Book();
        $book->setGoogleBooksId('123456789');
        $book->setTitle('Test Title');
        $book->setSubtitle('Test Subtitle');
        $book->setPublishedAt(new \DateTimeImmutable('2021-01-01'));
        $book->setDescription('Test Description');
        $book->setIsbn10('1234567890');
        $book->setIsbn13('1234567890123');
        $book->setPageCount(123);
        $book->setSmallThumbnail('https://example.com/small.jpg');
        $book->setThumbnail('https://example.com/large.jpg');
        $author = new Author();
        $book->addAuthor($author);
        $publisher = new Publisher();
        $book->addPublisher($publisher);
        $userBook = new UserBook();
        $book->addUserBook($userBook);


        $this->assertSame('123456789', $book->getGoogleBooksId());
        $this->assertSame('Test Title', $book->getTitle());
        $this->assertSame('Test Subtitle', $book->getSubtitle());
        $this->assertEquals(new \DateTimeImmutable('2021-01-01'), $book->getPublishedAt());
        $this->assertSame('Test Description', $book->getDescription());
        $this->assertSame('1234567890', $book->getIsbn10());
        $this->assertSame('1234567890123', $book->getIsbn13());
        $this->assertSame(123, $book->getPageCount());
        $this->assertSame('https://example.com/small.jpg', $book->getSmallThnmbnail());
        $this->assertSame('https://example.com/large.jpg', $book->getThumbnail());
        $this->assertSame($author, $book->getAuthors()->first());
        $this->assertSame($publisher, $book->getPublishers()->first());
        $this->assertSame($userBook, $book->getUserBooks()->first());
    }

    public function testAddAndRemoveAuthor(): void
    {
        $book = new Book();
        $author = new Author();
        $book->addAuthor($author);
        $this->assertSame($author, $book->getAuthors()->first());
        $book->removeAuthor($author);
        $this->assertEmpty($book->getAuthors());
    }

    public function testAddAndRemovePublisher(): void
    {
        $book = new Book();
        $publisher = new Publisher();
        $book->addPublisher($publisher);
        $this->assertSame($publisher, $book->getPublishers()->first());
        $book->removePublisher($publisher);
        $this->assertEmpty($book->getPublishers());
    }

    public function testAddAndRemoveUserBook(): void
    {
        $book = new Book();
        $userBook = new UserBook();
        $book->addUserBook($userBook);
        $this->assertSame($userBook, $book->getUserBooks()->first());
        $book->removeUserBook($userBook);
        $this->assertEmpty($book->getUserBooks());
    }
}