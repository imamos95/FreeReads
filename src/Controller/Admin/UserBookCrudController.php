<?php

namespace App\Controller\Admin;

use App\Entity\UserBook;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class UserBookCrudController extends AbstractCrudController
{
    use Trait\ReadOnlyTrait;

    public static function getEntityFqcn(): string
    {
        return UserBook::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->setLabel('ID'),
            TextField::new('user')->setLabel('Utilisateur'),
            TextField::new('book')->setLabel('Livre'),
            TextField::new('status')->setLabel('Statut'),
            TextEditorField::new('comment')->setLabel('Commentaire'),
            IntegerField::new('rating')->setLabel('Note'),
            DateField::new('createdAt')->setLabel('Date de création')->setFormat('d/m/Y'),
            DateField::new('updatedAt')->setLabel('Date de modification')->setFormat('d/m/Y'),
        ];
    }
}
