<?php

namespace App\Controller\Admin;

use App\Entity\Invitation;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class InvitationCrudController extends AbstractCrudController
{
    use Trait\CreateReadDeleteTrait;
    public static function getEntityFqcn(): string
    {
        return Invitation::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            EmailField::new('email')->setLabel('Email Address'),
            TextField::new('uuid')->setLabel('Uuid')->hideWhenCreating(),
            AssociationField::new('reader')->setLabel('Reader')->hideWhenCreating(),
        ];
    }

}
