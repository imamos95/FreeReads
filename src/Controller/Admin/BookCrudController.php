<?php

namespace App\Controller\Admin;

use App\Entity\Book;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class BookCrudController extends AbstractCrudController
{
    use Trait\ReadOnlyTrait;

    public static function getEntityFqcn(): string
    {
        return Book::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->setLabel('ID'),
            TextField::new('title')->setLabel('Titre'),
            TextField::new('subtitle')->setLabel('Sous-titre'),
            TextField::new('description')->setLabel('Description'),
            TextField::new('isbn10')->setLabel('ISBN 10'),
            TextField::new('isbn13')->setLabel('ISBN 13'),
            ImageField::new('smallThnmbnail')->setLabel('Petite image'),
            ImageField::new('thumbnail')->setLabel('Grande image'),
            CollectionField::new('authors')->setLabel('Auteurs'),
            CollectionField::new('publishers')->setLabel('Editeurs'),
            TextField::new('googleBooksId')->setLabel('ID Google Book'),
        ];
    }
}
