<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Publisher;
use App\Entity\Status;
use App\Entity\User;
use App\Entity\UserBook;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        // create 10 authors! Bam!

        $authors = [];

        for ($i = 0; $i < 10; ++$i) {
            $author = new Author();
            $author->setName($faker->name);
            $manager->persist($author);
            $authors[] = $author;
        }

        // create 10 publishers! Bam!
        $publishers = [];
        for ($i = 0; $i < 10; ++$i) {
            $publisher = new Publisher();
            $publisher->setName($faker->company);
            $manager->persist($publisher);
            $publishers[] = $publisher;
        }

        // create status

        $status = [];
        foreach (['to-read', 'reading', 'read'] as $value) {
            $oneStatus = new Status();
            $oneStatus->setName($value);
            $manager->persist($oneStatus);
            $status[] = $oneStatus;
        }

        // create 100 books! Bam!
        $books = [];
        for ($i = 0; $i < 100; ++$i) {
            $book = new Book();
            $book->setGoogleBooksId($faker->uuid);
            $book->setTitle($faker->sentence());
            $book->setSubtitle($faker->sentence());
            $book->setPublishedAt($faker->dateTimeBetween('-5 years', 'now'));
            $book->setDescription($faker->text());
            $book->setIsbn10($faker->isbn10());
            $book->setIsbn13($faker->isbn13());
            $book->setPageCount($faker->numberBetween(100, 1000));
            $book->setThumbnail('https://picsum.photos/200/300');
            $book->setSmallTnmbnail('https://picsum.photos/100/150');
            $book->addAuthor($faker->randomElement($authors));
            $book->addPublisher($faker->randomElement($publishers));

            $manager->persist($book);

            $books[] = $book;
        }

        // create 10 users! Bam!

        $users = [];
        for ($i = 0; $i < 10; ++$i) {
            $user = new User();
            $user->setEmail($faker->email);
            $user->setPassword($faker->password);
            $user->setPseudo($faker->userName);
            $user->setRoles(['ROLE_USER']);
            $manager->persist($user);
            $users[] = $user;
        }

        // create 10 userBook by User! Bam!
        foreach ($users as $user) {
            for ($i = 0; $i < 10; ++$i) {
                $userBook = new UserBook();
                $userBook
                    ->setUser($user)
                    ->setStatus($faker->randomElement($status))
                    ->setRating($faker->numberBetween(0, 5))
                    ->setComment($faker->text())
                    ->setBook($faker->randomElement($books))
                    ->setCreatedAt(DateTimeImmutable::createFromMutable($faker->dateTime()));
                $userBook->setUpdatedAt(DateTimeImmutable::createFromMutable($faker->dateTime()));

                $manager->persist($userBook);
            }
        }

        $manager->flush();
    }
}
